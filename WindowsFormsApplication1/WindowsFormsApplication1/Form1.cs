﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Milchama : Form
    {

        private float numberOfCards = 10; // put how many cards as you want, it will stretch! (:

        Thread listener;
        NetworkStream clientNetworkStream = null;
        private delegate void milchamaOperation();
        private event milchamaOperation milchamaEvent;
        private string oponentCardString = null;
        private int myCardValue = 0;
        bool userCardSelected = false;
        private int myScoreValue = 0, hisScoreValue = 0;

        private bool running = true;

        List<System.Windows.Forms.PictureBox> cards;

        public Milchama()
        {
            InitializeComponent();

            cards = new List<PictureBox>();
            listener = new Thread(dealingWithServer);
            listener.Start();

            setAbleState(false);

            running = true;
        }

        private void quitButtonOnCLick(object sender, EventArgs e)
        {
            // set running as false, the listener thread will finish the game
            running = false;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void quitGame()
        {
            clientNetworkStream.Close(); // Close the connection with the server

            // Show a message
            MessageBox.Show("The game is finished.\nYour score is: " + myScoreValue.ToString() + "\nOponent's score is: " + hisScoreValue.ToString());

            // quit!
            Invoke((MethodInvoker)delegate
            {
                this.Close();
            });

            // kill the server listener/worker thread
            listener.Abort();
        }

        private void doMilchama()
        {

            milchamaEvent -= doMilchama;

            string localOponentsCardString;
            lock (oponentCardString)
            {
                localOponentsCardString = oponentCardString;
                oponentCardString = null;
            }

            string numberName = null;
            if (localOponentsCardString[1] == '0') numberName = "_" + localOponentsCardString[2].ToString();
            else numberName = "_" + localOponentsCardString[1].ToString() + localOponentsCardString[2].ToString();

            string letter = localOponentsCardString[3].ToString();
            string typeName = "";

            if (letter == "C") { typeName = "clubs"; };
            if (letter == "H") { typeName = "hearts"; }
            if (letter == "S") { typeName = "spades"; }
            if (letter == "D") { typeName = "diamonds"; }

            if (numberName == "_1") numberName = "ace";
            else if (numberName == "_11") numberName = "jack";
            else if (numberName == "_12") numberName = "queen";
            else if (numberName == "_13") numberName = "king";

            object o = Properties.Resources.ResourceManager.GetObject(numberName + "_of_" + typeName);
            oponentsCard.Image = (Bitmap)o;

            int oponentsCardValue = 10 * (localOponentsCardString[1] - '0') + (localOponentsCardString[2] - '0');

            if (oponentsCardValue > myCardValue)
                hisScoreValue++;
            else if (oponentsCardValue != myCardValue)
                myScoreValue++;


            Invoke((MethodInvoker)delegate
            {
                hisScoreValueText.Text = hisScoreValue.ToString();
                myScoreValueText.Text = myScoreValue.ToString();
            });

            userCardSelected = false;
        }

        private void clearAllCards()
        {
            oponentsCard.Image = global::WindowsFormsApplication1.Properties.Resources.card_back_blue;
            foreach (PictureBox card in cards)
            {
                card.Image = global::WindowsFormsApplication1.Properties.Resources.card_back_red;
            }
        }

        private void GenerateCards()
        {

            float cardWidth, cardHeight, marginRate, margin;
            float cardsYPos;

            marginRate = 0.1f;
            cardWidth = (this.Size.Width * (1 - marginRate)) / (numberOfCards);
            margin = this.Size.Width * marginRate / (numberOfCards + 2); // adding 2 for additional margin on both sides
            cardHeight = this.Size.Height * 0.25f;
            cardsYPos = this.Size.Height * 0.9f - cardHeight; // 0.9 is the "distance rate" from the bottom

            // setting the card setting point at the beggining
            Point setPoint = new Point((int)(margin), (int)cardsYPos);

            // creating cards
            for (int i = 0; i < numberOfCards; i++)
            {
                // create image
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "card number " + i;
                currentPic.Image = global::WindowsFormsApplication1.Properties.Resources.card_back_red;
                currentPic.Location = setPoint;
                currentPic.Size = new System.Drawing.Size((int)cardWidth, (int)cardHeight);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                // add the event
                currentPic.Click += cardsOnClickEvent;

                // add card to form
                this.Controls.Add(currentPic);

                setPoint.X += (int)(margin + cardWidth);

                cards.Add(currentPic);
            }
        }

        private void cardsOnClickEvent(object sender, EventArgs e)
        {

            if (clientNetworkStream == null) return; // shouldn't happend because of the button's locking, but just to make sure...

            clearAllCards();

            PictureBox pic = (PictureBox)sender;
            Random random = new Random();

            // generating a random card
            int number = random.Next(1, 13), letterNum = random.Next(1, 4);
            string letter = "", typeName = "", numberName;

            if (letterNum == 1) { letter = "C"; typeName = "clubs"; };
            if (letterNum == 2) { letter = "H"; typeName = "hearts"; }
            if (letterNum == 3) { letter = "S"; typeName = "spades"; }
            if (letterNum == 4) { letter = "D"; typeName = "diamonds"; }

            if (number == 1) numberName = "ace";
            else if (number == 11) numberName = "jack";
            else if (number == 12) numberName = "queen";
            else if (number == 13) numberName = "king";
            else numberName = "_" + number.ToString();

            // getting the card image and setting the selected card's image
            object o = Properties.Resources.ResourceManager.GetObject(numberName + "_of_" + typeName);
            pic.Image = (Bitmap)o;

            // sending the card message to the server
            string cardNumberString = "";
            if (number < 10)
                cardNumberString = "0" + number.ToString();
            else
                cardNumberString = number.ToString();
            byte[] message = new ASCIIEncoding().GetBytes("1" + cardNumberString + letter);
            lock (clientNetworkStream)
            {
                try
                {
                    clientNetworkStream.Write(message, 0, 4);
                    clientNetworkStream.Flush();
                }
                catch (Exception sendingException)
                {
                    Console.WriteLine(sendingException.ToString());
                }
            }

            userCardSelected = true;
            myCardValue = number;

            try
            {
                milchamaEvent(); // do the milchama!
            }
            catch (NullReferenceException callingException)
            {
                // the event was null...
                Console.WriteLine(callingException.ToString());
            }
        }

        private void dealingWithServer()
        {
            byte[] buffer = new byte[4];
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);


            string input = "";

            while (input != "0000")
            {
                try
                {
                    client.Connect(serverEndPoint);
                    NetworkStream clientStream = client.GetStream();
                    clientNetworkStream = clientStream;
                    int bytesRead = clientStream.Read(buffer, 0, 4);
                    input = new ASCIIEncoding().GetString(buffer);
                }
                catch (Exception e)
                {
                    // If reached here - the server is probably not set up
                }

            }

            while (!InvokeRequired) ; // wait for the UI to set up properly (if hasn't been done yet...)
            Invoke((MethodInvoker)delegate { GenerateCards(); setAbleState(true); });

            // start the game!
            while (input != "2000" && running)
            {
                if (clientNetworkStream.DataAvailable)
                {
                    int bytesRead = clientNetworkStream.Read(buffer, 0, 4);
                    input = new ASCIIEncoding().GetString(buffer);

                    // check if the input is a card message
                    if (input[0] == '1')
                    {
                        oponentCardString = input;
                        milchamaEvent += doMilchama;
                        if (userCardSelected) milchamaEvent();
                    }
                }
            }

            // send a request to end the game
            byte[] message = new ASCIIEncoding().GetBytes("2000");
            lock (clientNetworkStream)
            {
                try
                {
                    clientNetworkStream.Write(message, 0, 4);
                    clientNetworkStream.Flush();
                }
                catch (Exception sendingException)
                {
                    Console.WriteLine(sendingException.ToString());
                }
            }

            // if reached here, then the server requested to end the game.
            quitGame();
        }

        private void setAbleState(bool state)
        {
            foreach (PictureBox p in cards)
            {
                p.Enabled = state;
            }
            button1.Enabled = state;
        }
    }
}
