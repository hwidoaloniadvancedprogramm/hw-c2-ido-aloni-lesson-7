﻿namespace WindowsFormsApplication1
{
    partial class Milchama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myScore = new System.Windows.Forms.Label();
            this.hisScore = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.oponentsCard = new System.Windows.Forms.PictureBox();
            this.myScoreValueText = new System.Windows.Forms.Label();
            this.hisScoreValueText = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.oponentsCard)).BeginInit();
            this.SuspendLayout();
            // 
            // myScore
            // 
            this.myScore.AutoSize = true;
            this.myScore.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.myScore.Location = new System.Drawing.Point(133, 50);
            this.myScore.Name = "myScore";
            this.myScore.Size = new System.Drawing.Size(140, 27);
            this.myScore.TabIndex = 0;
            this.myScore.Text = "Your score: ";
            this.myScore.Click += new System.EventHandler(this.label1_Click);
            // 
            // hisScore
            // 
            this.hisScore.AutoSize = true;
            this.hisScore.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.hisScore.Location = new System.Drawing.Point(886, 50);
            this.hisScore.Name = "hisScore";
            this.hisScore.Size = new System.Drawing.Size(202, 27);
            this.hisScore.TabIndex = 1;
            this.hisScore.Text = "Oponent\'s score: ";
            // 
            // button1
            // 
            this.button1.AccessibleName = "QuitButton";
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(609, 55);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 31);
            this.button1.TabIndex = 2;
            this.button1.Text = "Quit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.quitButtonOnCLick);
            // 
            // oponentsCard
            // 
            this.oponentsCard.AccessibleName = "OponentsCard";
            this.oponentsCard.Image = global::WindowsFormsApplication1.Properties.Resources.card_back_blue;
            this.oponentsCard.Location = new System.Drawing.Point(563, 130);
            this.oponentsCard.Name = "oponentsCard";
            this.oponentsCard.Size = new System.Drawing.Size(160, 204);
            this.oponentsCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.oponentsCard.TabIndex = 3;
            this.oponentsCard.TabStop = false;
            // 
            // myScoreValueText
            // 
            this.myScoreValueText.AccessibleName = "YourScore";
            this.myScoreValueText.AutoSize = true;
            this.myScoreValueText.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.myScoreValueText.Location = new System.Drawing.Point(302, 50);
            this.myScoreValueText.Name = "myScoreValueText";
            this.myScoreValueText.Size = new System.Drawing.Size(25, 27);
            this.myScoreValueText.TabIndex = 4;
            this.myScoreValueText.Text = "0";
            // 
            // hisScoreValueText
            // 
            this.hisScoreValueText.AccessibleName = "OponentsScore";
            this.hisScoreValueText.AutoSize = true;
            this.hisScoreValueText.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.hisScoreValueText.Location = new System.Drawing.Point(1157, 50);
            this.hisScoreValueText.Name = "hisScoreValueText";
            this.hisScoreValueText.Size = new System.Drawing.Size(25, 27);
            this.hisScoreValueText.TabIndex = 5;
            this.hisScoreValueText.Text = "0";
            // 
            // Milchama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1320, 756);
            this.Controls.Add(this.hisScoreValueText);
            this.Controls.Add(this.myScoreValueText);
            this.Controls.Add(this.oponentsCard);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.hisScore);
            this.Controls.Add(this.myScore);
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Name = "Milchama";
            this.Text = "Milchama";
            ((System.ComponentModel.ISupportInitialize)(this.oponentsCard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label myScore;
        private System.Windows.Forms.Label hisScore;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox oponentsCard;
        private System.Windows.Forms.Label myScoreValueText;
        private System.Windows.Forms.Label hisScoreValueText;
    }
}

